<?php
/**
 * Roxed by : Yorick
 * User: yferlin
 * Date: 27/01/2020
 */

namespace App\DataFixtures;

use App\Entity\Grade;
use App\Entity\Student;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class StudentFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $disciplines = ['contorsion', 'evasion', 'ronds_de_jambes', 'pipeau', 'tir_aux_pigeons'];
        $faker = Factory::create();
        for ($i=0; $i < 40; $i++) {
            $student = new Student();
            $student->setName($faker->name)
                ->setFirstName($faker->firstName)
                ->setBirthDate($faker->dateTime)
            ;

            for ($j=0; $j < 5; $j++) {
                $grade = new Grade();
                $grade->setDiscipline($disciplines[$j])
                    ->setStudent($student)
                    ->setValue($faker->numberBetween(0,20))
                ;
                $manager->persist($grade);
            }

            $manager->persist($student);
        }
        $manager->flush();
    }
}
<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GradeRepository")
 *
 */
class Grade
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"studentSingle"})
     */
    private $id;

    /**
     * @ORM\Column(type="smallint")
     * @Assert\NotNull(
     *     message="Le champ {{ value }} ne doît pas être null"
     * )
     * @Assert\NotBlank(
     *     message="le champ {{ value }} ne doît pas être vide"
     * )
     * @Assert\Range(
     *     min=0,
     *     max=20,
     *     minMessage="La note minimale doît être de 0",
     *     maxMessage="La note maximale doît être de 20"
     * )
     * @Groups({"studentSingle", "gradePostData"})
     */
    private $value;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotNull(
     *     message="Le champ {{ value }} ne doît pas être null"
     * )
     * @Assert\NotBlank(
     *     message="le champ {{ value }} ne doît pas être vide"
     * )
     * @Groups({"studentSingle", "gradePostData"})
     */
    private $discipline;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Student", inversedBy="grades")
     * @ORM\JoinColumn(nullable=false)
     *
     */
    private $student;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValue(): ?int
    {
        return $this->value;
    }

    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getDiscipline(): ?string
    {
        return $this->discipline;
    }

    public function setDiscipline(string $discipline): self
    {
        $this->discipline = $discipline;

        return $this;
    }

    public function getStudent(): ?Student
    {
        return $this->student;
    }

    public function setStudent(?Student $student): self
    {
        $this->student = $student;

        return $this;
    }
}

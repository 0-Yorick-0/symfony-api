<?php
/**
 * Roxed by : Yorick
 * Date: 02/02/2020
 */

namespace App\Subscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Serializer\SerializerInterface;

class ExceptionSubscriber implements EventSubscriberInterface
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => [
                'processException', 255
            ],
        ];
    }

    /**
     * Sérialisation des exceptions en JSON afin qu'en cas d'exception un message de type {code : [code], message: [message]}
     * soit retourné
     *
     * @param ExceptionEvent $event
     */
    public function processException(ExceptionEvent $event)
    {
        //récupération de l'exception courante
        $exception = $event->getThrowable();
        $response = new JsonResponse();

        if ($exception instanceof NotFoundHttpException) {
            $response->setStatusCode(Response::HTTP_NOT_FOUND);
            $response->headers->replace($exception->getHeaders());

            $content = [
                'code' => Response::HTTP_NOT_FOUND,
                'message' => $exception->getMessage()
            ];

            $response->setContent($this->serializer->serialize($content, 'json'));
        }else{
            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $event->setResponse($response);
    }
}
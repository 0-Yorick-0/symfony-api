<?php
/**
 * Roxed by : Yorick
 * Date: 28/01/2020
 */

namespace App\Controller;

use App\Entity\Grade;
use App\Entity\Student;
use App\Repository\GradeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class GradeController
 * @Route("/api/v1")
 * @SWG\Tag(name="Notes")
 */
class GradeController extends AbstractController
{
    /**
     * @Route("/grades/average", name="grades_average", methods={"GET"})
     * @SWG\Response(
     *     response=200,
     *     description="Moyenne de toutes les notes de la classe",
     * )
     */
    public function average(GradeRepository $repository)
    {
        $data = [
            'class_average' => $repository->getClassAverage()
        ];
        return new JsonResponse($data, Response::HTTP_OK, []);
    }

    /**
     * @Route("/students/{id}/grades", methods={"POST"}, name="add_grades")
     * @SWG\Response(
     *     response=201,
     *     description="Permet d'ajouter une note à un élève",
     *     @Model(type=Grade::class)
     * )
     * @SWG\Parameter(
     *     name="note",
     *     in="body",
     *     @Model(type=Grade::class, groups={"gradePostData"}),
     *     description="Création de l'élève"
     * )
     */
    public function create(Student $student, Request $request, SerializerInterface $serializer, ValidatorInterface $validator, EntityManagerInterface $em, GradeRepository $repository)
    {
        //désérialisation des données pour en fait un objet de type Grade
        $grade = $serializer->deserialize($request->getContent(), Grade::class, 'json');
        //on valide les données
        $errors = $validator->validate($grade);
        if (count($errors)) {
            //en cas d'erreur, on retourne les erreurs à l'utilisateur
            return new JsonResponse(
                $serializer->serialize($errors, 'json'),
                Response::HTTP_BAD_REQUEST,
                [],
                true
            );
        }
        $oldGrade = $repository->findOneBy([
            'student' => $student->getId(),
            'discipline' => $grade->getDiscipline(),
        ]);
        //on retire l'ancienne note si la note créée l'est dans une discipline existant déjà
        if ($oldGrade) {
            $student->removeGrade($oldGrade);
        }

        $student->addGrade($grade);
        $em->persist($student);
        //si tout va bien, on enregistre l'entité en base
        $em->persist($grade->setStudent($student));
        $em->flush();
        //en cas de succès on ne renvoie rien, avec un code 201 et l'url de la ressource crée dans l'entête 'Location'
        return new JsonResponse(
            null,
            Response::HTTP_CREATED,
            ['Location' => $this->generateUrl('get_students', ['id' => $student->getId()])]
        );
    }
}

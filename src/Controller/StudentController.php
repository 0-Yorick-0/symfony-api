<?php
/**
 * Roxed by : Yorick
 * Date: 28/01/2020
 */

namespace App\Controller;

use App\Entity\Student;
use App\Repository\GradeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class StudentController
 * @Route("/api/v1")
 * @SWG\Tag(name="Elèves")
 */
class StudentController extends AbstractController
{
    /**
     * @Route("/students/{id}", methods={"GET"}, name="get_students")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Renvoie un élève et ses notes s'il en a",
     *     @Model(type=Student::class)
     * )
     */
    public function show(Student $student, SerializerInterface $serializer)
    {
        //lors de la sérialisation, le troisième paramètre définit le contexte de désérialisation de l'entité
        //c'est-à-dire qu'on ne prend en compte que les champs présentant l'annotation @studentSingle
        $student = $serializer->serialize($student, 'json', [AbstractNormalizer::GROUPS => 'studentSingle']);

        return new JsonResponse($student, Response::HTTP_OK, [], true);
    }

    /**
     * @Route("/students/{id}/average", methods={"GET"}, name="get_students_average")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Moyenne des notes d'un élève",
     * )
     */
    public function studentAverage(Student $student, SerializerInterface $serializer, GradeRepository $gradeRepository)
    {
        $data = [
            'student_average' => $gradeRepository->getStudentAverage($student->getId())
        ];
        return new JsonResponse($data, Response::HTTP_OK, []);
    }

    /**
     * @Route("/students", methods={"POST"})
     * @SWG\Response(
     *     response=201,
     *     description="Permet de créer un élève",
     *     @Model(type=Student::class)
     * )
     * @SWG\Parameter(
     *     name="élève",
     *     in="body",
     *     @Model(type=Student::class, groups={"userPostData"}),
     *     description="Création de l'élève"
     * )
     */
    public function create(Request $request, SerializerInterface $serializer, ValidatorInterface $validator, EntityManagerInterface $em)
    {
        //désérialisation des données pour en fait un objet de type Student
        $student = $serializer->deserialize($request->getContent(), Student::class, 'json');
        //on valide les données
        $errors = $validator->validate($student);
        if (count($errors)) {
            //en cas d'erreur, on retourne les erreurs à l'utilisateur
            return new JsonResponse(
              $serializer->serialize($errors, 'json'),
              Response::HTTP_BAD_REQUEST,
            [],
            true
            );
        }
        //si tout va bien, on enregistre l'entité en base
        $em->persist($student);

        $em->flush();
        //en cas de succès on ne renvoie rien, avec un code 201 et l'url de la ressource crée dans l'entête 'Location'
        return new JsonResponse(
            null,
            Response::HTTP_CREATED,
            ['Location' => $this->generateUrl('get_students', ['id' => $student->getId()])]
        );
    }

    /**
     * @Route("/students/{id}", methods={"PATCH"})
     * @SWG\Response(
     *     response=204,
     *     description="Permet de modifier un élève",
     *     @Model(type=Student::class)
     * )
     * @SWG\Parameter(
     *     name="student",
     *     in="body",
     *     @Model(type=Student::class, groups={"userPostData"}),
     *     description="Modification de l'élève"
     * )
     */
    public function update(Student $student, Request $request, SerializerInterface $serializer, ValidatorInterface $validator, EntityManagerInterface $em)
    {
        //désérialisation des données utilisateurs. Le dernier param indique qu'on modifie un objet qui existe déjà, car nous sommes dans
        // un contexte d'update
        $data = $serializer->deserialize($request->getContent(), Student::class, 'json', [AbstractNormalizer::OBJECT_TO_POPULATE => $student]);

        $errors = $validator->validate($student);
        if (count($errors)) {
            //en cas d'erreur, on retourne les erreurs à l'utilisateur
            return new JsonResponse(
                $serializer->serialize($errors, 'json'),
                Response::HTTP_BAD_REQUEST,
                [],
                true
            );
        }

        $em->persist($student);
        $em->flush();
        //en cas de succès on ne renvoie rien, avec un code 201 et l'url de la ressource crée dans l'entête 'Location'
        return new JsonResponse(
            null,
            Response::HTTP_NO_CONTENT,
            ['Location' => $this->generateUrl('get_students', ['id' => $student->getId()])]
        );
    }

    /**
     * @Route(
     *     "/students/{id}",
     *      methods={"DELETE"},
     *     requirements={"id"="\d+"}
     * )
     * @SWG\Response(
     *     response=204,
     *     description="Suppression d'un élève et de ses notes",
     *     @Model(type=Student::class)
     * )
     */
    public function delete(Student $student)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($student);
        $em->flush();

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
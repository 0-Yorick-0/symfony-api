# Api de notation d'élève

Avant toute chose, sachez qu'il est disponible de récupérer l'api via git puis de la **déployer** sur un **serveur local**. Des **fixtures** et des **migrations** sont disponibles, afin que l'api soit testable rapidement.

De même, un swagger est disponible à l'adresser `/api/doc`, déployé à l'aide du bundle `nelmio/api-doc-bundle`


## Cahier des charges :

L'api permet de : 
* Lire les informations d'un élève => [`GET /api/v1/students/{id}`][get_students]
* Créer un élève => [`POST /api/v1/students`][add_students]
* Modifier les informations d'un élève => [`PATCH /api/v1/students/{id}`][update_students]
* Supprimer un élève => [`DELETE /api/v1/students/{id}`][delete_students]
* Lire la moyenne des notes d'un élève => [`GET /api/v1/students/{id}`][students_average]
* Ajouter une note à un élève => [`POST /api/v1/grades`][add_grades]
* Lire la moyenne de la classe => [`GET /api/v1/grades/average`][grades_average]

```
Gestion des Exceptions : Les exceptions sont retournées sous la forme :
{
    code: [code],
    message: [message]
} 
```


### Lire les informations d un eleve

`GET /api/v1/students/{id}`

#### Requête :

1. Paramètres :
* id : (int) *required => id de l'élève

#### Réponse :

1. Code : 200

```JSON
{
  "id": 0,
  "name": "string",
  "firstName": "string",
  "grades": [
    {
      "id": 0,
      "value": 0,
      "discipline": "string"
    }
  ],
  "birthDate": "2020-02-04T10:51:55.746Z"
}
```

### Créer un eleve

`POST /api/v1/students`


#### Requête :

```JSON
{
  "name": "string",(required)
  "firstName": "string",(required)
  "birthDate": "2020-02-04",(required)
}
```


#### Réponse :


Code : 201


### Modifier les informations d'un eleve

`PATCH /api/v1/students/{id}`


#### Requête :
```JSON
{
  "name": "string",
  "firstName": "string",
  "birthDate": "2020-02-04",
}
```


#### Réponse :


Code : 204

### Supprimer un eleve


`DELETE /api/v1/students/{id}`


#### Requête :
1. Paramètres :
* id : (int) *required => id de l'élève

#### Réponse :

Code : 204

### Lire la moyenne des notes d un eleve


`GET /api/v1/students/{id}`


#### Requête :
1. Paramètres :
* id : (int) *required => id de l'élève

#### Réponse :

Code: 200

Body:

```JSON
{
  "student_average": "float"
}
```


### Ajouter une note à un eleve

`POST /api/v1/grades`


#### Requête:

1. Paramètres :
* id : (int) *required => id de l'élève

Body:

```JSON
{
  "value": 0,
  "discipline": "string"
}
```


#### Réponse:

Code: 201

### Lire la moyenne de la classe

`GET /api/v1/grades/average`


#### Réponse :
Code: 200

Body:

```JSON
{
  "class_average": "float"
}
```



[get_students]: #lire-les-informations-d-un-eleve
[add_students]: #lire-les-informations-d-un-eleve
[update_students]: #lire-les-informations-d-un-eleve
[delete_students]: #lire-les-informations-d-un-eleve
[students_average]: #lire-les-informations-d-un-eleve
[add_grades]: #lire-les-informations-d-un-eleve
[grades_average]: #lire-les-informations-d-un-eleve